<?php
require_once("registration/libraries/password_compatibility_library.php");
require_once("registration/config/db.php");
require_once("registration/config/hashing.php");
require_once("registration/classes/Login.php");
?>


<!--
 =========================================================
 * Material Kit - v2.0.6
 =========================================================

 * Product Page: https://www.creative-tim.com/product/material-kit
 * Copyright 2019 Creative Tim (http://www.creative-tim.com)
   Licensed under MIT (https://github.com/creativetimofficial/material-kit/blob/master/LICENSE.md)


 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="./assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Material Kit by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- CSS Files -->
  <link href="/assets/css/material-kit.min.css?v=2.0.6" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="/assets/demo/demo.css" rel="stylesheet" />
  <link href="/assets/css/styles.css" rel="stylesheet" />
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body class="index-page sidebar-collapse">
  <nav class="navbar fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="https://demos.creative-tim.com/material-kit/index.html">
          Material Kit </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
          <li class="dropdown nav-item">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
              <i class="material-icons">apps</i> Components
            </a>
            <div class="dropdown-menu dropdown-with-icons">
              <a href="./index.html" class="dropdown-item">
                <i class="material-icons">layers</i> All Components
              </a>
              <a href="https://demos.creative-tim.com/material-kit/docs/2.1/getting-started/introduction.html" class="dropdown-item">
                <i class="material-icons">content_paste</i> Documentation
              </a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" onclick="scrollToDownload()">
              <i class="material-icons">cloud_download</i> Download
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank" data-original-title="Follow us on Twitter">
              <i class="fa fa-twitter"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank" data-original-title="Like us on Facebook">
              <i class="fa fa-facebook-square"></i>
            </a>
          </li>
          <li class="nav-item">
         <?php $login = new Login();

// ... ask if we are logged in here:
if ($login->isUserLoggedIn() == true) {
    // the user is logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are logged in" view.
    echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addServerModal">
    Dodaj Serwer </button>';
    echo $_SESSION['user_name'];
    echo '<a href="index.php?logout">Logout</a>';

} else {
  echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#loginModal">
  Zaloguj </button>
 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#registerModal">
Zarejestruj
</button>';
}
?>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--Modal z dodawaniem-->
  <div class="modal fade" id="addServerModal" tabindex="-1" role="dialog" aria-labelledby="addServerModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addServerModalLabel">Dodwanie serwera</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php
        require 'inc/dbconnect.php';
            if ($login->isUserLoggedIn() == true) {
        		echo '<form method="POST"><div class="form-group">
                <label for="trescset">Wprowadź Tytuł</label>
                <input type="text" name="ip" style="width: 500px;" class="form-control" id="trescset"  placeholder="Tytuł">
              </div><br/><br/>';
            echo '
      <div class="g-recaptcha" data-sitekey="6Lei5MMUAAAAABlGbpLQ0Lbfl0tyGF3BaJ5S1GnB"></div>
      <br/>
      <input type="submit" value="Submit" name="dodaj" class="btn btn-success btn-round">
    </form>';
        		}
            if (isset($_POST['dodaj'])) {
        		$ip = $_POST['ip'];
        		$sql = mysqli_query($conn, "INSERT INTO serwery(id,ip) VALUES('id','".$ip."')");
        	    }
        ?>
      </div>
    </div>
  </div>
</div>
<!-- Koniec z dodawaniem -->
<!--Modal z logowaniem -->
  <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Logowanie</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="index.php" name="loginform">
    <label for="login_input_username">Username</label><br/>
    <input id="login_input_username" class="login_input" type="text" name="user_name" required /><br/><br/>
    <label for="login_input_password">Password</label><br/>
    <input id="login_input_password" class="login_input" type="password" name="user_password" autocomplete="off" required /><br/><br/>
    <input type="submit"  name="login" value="Log in" /><br/><br/>
</form>
      </div>
      <div class="modal-footer">
        <button type="button" value="Log in" name="login" class="btn btn-primary">Zaloguj</button>
      </div>
    </div>
  </div>
</div>
<!-- Koniec z logowaniem -->
<!--Modal z rejestracja -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="registerModalLabel">Rejestracja</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="registration/register.php" name="registerform">

    <!-- NOTE: those <br/> are bad style and only there for basic formatting. remove them when you use real .css -->

    <!-- the user name input field uses a HTML5 pattern check -->
    <label for="login_input_username">Username (only letters and numbers, 2 to 64 characters)</label><br/>
    <input id="login_input_username" class="login_input" type="text" pattern="[a-zA-Z0-9]{2,64}" name="user_name" required /><br/><br/>

    <!-- the email input field uses a HTML5 email type check -->
    <label for="login_input_email">User's email</label><br/>
    <input id="login_input_email" class="login_input" type="email" name="user_email" required /><br/><br/>

    <label for="login_input_password_new">
        Password (min. 6 characters!<br/>
        Please note: using a long sentence as a password is much much safer then something like "!c00lPa$$w0rd").<br/>
        Have a look on <a href="http://security.stackexchange.com/questions/6095/xkcd-936-short-complex-password-or-long-dictionary-passphrase">this interesting security.stackoverflow.com thread</a>.
    </label><br/>
    <input id="login_input_password_new" class="login_input" type="password" name="user_password_new" pattern=".{6,}" required autocomplete="off" /><br/><br/>

    <label for="login_input_password_repeat">Repeat password</label><br/>
    <input id="login_input_password_repeat" class="login_input" type="password" name="user_password_repeat" pattern=".{6,}" required autocomplete="off" /><br/><br/>

    <!-- generate and display a captcha and write the captcha string into session -->
    <img src="registration/tools/showCaptcha.php" /><br/>

    <label>Please enter those characters</label><br/>
    <input type="text" name="captcha" /><br/><br/>


      </div>
      <div class="modal-footer">
      <input class="btn btn-primary" type="submit"  name="register" value="Register" />
      </form>
      </div>
    </div>
  </div>
</div>
<!-- Koniec z rejestracja -->

  <div class="main main-raised top-margin">
    <div class="section section-basic">
      <div id="website-content" class="container text-center mt-md-4 px-md-5">


<div class="py-3 pb-5">
<div class="row">

<div class="col-md-12">
<div class="mb-1 server-list-entry">
<div class="text-black-60 row text-center d-flex align-items-center py-1 px-3 h-100">
<div class="col-lg-8 col-md-6 col-sm-12 d-flex align-items-center">
Podstawowe informacje
</div>
<div class="col-lg-4 col-md-6 col-sm-12">
<div class="row">
<div class="text-nowrap col-md-4 d-flex align-items-center" style="width: 33.3%">
Wersja
</div>
<div class="text-nowrap col-md-5 text-right" style="width: 43.3%">
Online
</div>
<div class="text-nowrap col-md-3 text-right" style="width: 23.3%">
<i class="fa fa-thumbs-o-up"></i>
</div>
</div>
</div>
</div>
</div>
</div>
      <div class="container">
      <div class="row">
        <?php
        require 'inc/dbconnect.php';
$pobierz = "SELECT ip FROM serwery";
$pobierz .= " ORDER BY id DESC";
$result = mysqli_query($conn, $pobierz);
if (mysqli_num_rows($result) > 0) {
while($row = mysqli_fetch_assoc($result)) {
  $status = json_decode(file_get_contents('https://api.mcsrvstat.us/2/'.$row['ip'].''));
  echo '<div class="container">';
  echo '<div class="row justify-content-md-center">';
  echo '<div class="col col-lg-2">';
      echo '<img src="'.$status->icon.'" height="32" width="32">'.$row['ip'].'';
    echo '</div>';
    echo '<div class="col-md-auto">';
    foreach ($status->motd->clean as $motd) {
	echo $motd;
}

    echo '</div>';
    echo '<div class="col col-lg-2">';
    echo $status->version;
    echo '</div>';
  echo '</div>';

}}
?>
 </div>
      </div>
    </div>
  </div>
  <!--  End Modal -->
  <footer class="footer" data-background-color="black">
    <div class="container">
      <nav class="float-left">
        <ul>
          <li>
            <a href="https://www.creative-tim.com">
              Creative Tim
            </a>
          </li>
          <li>
            <a href="https://creative-tim.com/presentation">
              About Us
            </a>
          </li>
          <li>
            <a href="http://blog.creative-tim.com">
              Blog
            </a>
          </li>
          <li>
            <a href="https://www.creative-tim.com/license">
              Licenses
            </a>
          </li>
        </ul>
      </nav>
      <div class="copyright float-right">
        &copy;
        <script>
          document.write(new Date().getFullYear())
        </script>, made with <i class="material-icons">favorite</i> by
        <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
      </div>
    </div>
  </footer>
<!--   Core JS Files   -->
<script
  src="https://demos.creative-tim.com/material-kit/assets/js/core/jquery.min.js"
  crossorigin="anonymous"></script>

  <script src="./assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="./assets/js/plugins/moment.min.js"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="./assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="https://demos.creative-tim.com/material-kit/assets/js/plugins/nouislider.min.js" crossorigin="anonymous" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!--	Plugin for Sharrre btn -->
  <script src="./assets/js/plugins/jquery.sharrre.js" type="text/javascript"></script>
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/material-kit.min.js?v=2.0.6" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //init DateTimePickers
      materialKit.initFormExtendedDatetimepickers();

      // Sliders Init
      materialKit.initSliders();
    });


    function scrollToDownload() {
      if ($('.section-download').length != 0) {
        $("html, body").animate({
          scrollTop: $('.section-download').offset().top
        }, 1000);
      }
    }

  </script>
</body>

</html>
